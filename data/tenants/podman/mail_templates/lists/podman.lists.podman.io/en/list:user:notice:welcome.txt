This list is for your questions, concerns or comments about the Podman project (https://github.com/containers/libpod).  The list will also be used by the maintainers when there are announcements of interest to the community.    To send an email to the list, simply send an email to podman@lists.podman.io with an appropriate subject.  To see the archives or to manage your subscription, go to https://lists.podman.io/archives/list/podman@lists.podman.io/.

Podman is an open-source project that is available on most Linux platforms and resides on GitHub.  Podman is a daemonless container engine for developing, managing, and running Open Container Initiative (OCI) containers and container images on your Linux System.  Podman provides a Docker-compatible command line front end that can simply alias the Docker command-line-interface, `alias docker=podman`.  Containers under the control of Podman can either be run by root or by a non-privileged user.  Podman manages the entire container ecosystem which includes pods, containers, container images, and container volumes using the libpod library.

Podman has a web site (https://www.podman.io) devoted to blogs and announcements for the project along with tutorials and slides from talks that the maintainers have made at a variety of conferences.   In addition to the website and mailing lists, many of the maintainers monitor the #podman channel on the freenode IRC network.

Podman has a sister project, Buildah, which specializes in the building of container images and containers from ‘scratch’ or by using command line tools or industry standard Dockerfiles.  Buildah has its own website (https://www.buildah.io) that you can check out for more information.

Discussions about issues with Podman on this list are welcomed.  If you are using a distribution package of Podman, be sure to file a bug against the distribution when you have problems.  Otherwise, if you are using upstream code from our GitHub repository,  you'll need to raise an issue in GitHub (https://github.com/containers/libpod/issues).

We're happy to have you join our community and look forward to your contributions!
