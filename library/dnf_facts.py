#!/usr/bin/python
# coding: utf-8 -*-

# © 2018 Marc Dequènes (Duck) <duck@redhat.com>
#
# This module is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this software.  If not, see <http://www.gnu.org/licenses/>.


# this is magic, see lib/ansible/module_common.py
from ansible.module_utils.basic import *   # noqa: F403
import dnf.const


DOCUMENTATION = '''
---
module: dnf_facts
short_description: Returns DNF information
version_added:
options:
'''

EXAMPLES = '''
- name: Gather information about DNF
  dnf_facts:

- name: Display the DNF plugin installation path
  debug:
    msg: "DNF plugin directory: {{ dnf_plugin_path }}"
'''

RETURN = '''
dnf_plugin_path:
    description: path where DNF plugins are stored
    type: string
    returned: always
dnf_plugin_conf_path:
    description: path where DNF plugins' configuration files are stored
    type: string
    returned: always
'''


def main():
    module = AnsibleModule(   # noqa: F405
        argument_spec=dict(
        ),
    )

    ansible_facts = {
        'dnf_plugin_path': dnf.const.PLUGINPATH,
        'dnf_plugin_conf_path': dnf.const.PLUGINCONFPATH
    }

    module.exit_json(changed=False, ansible_facts=ansible_facts)


main()
