---

- import_playbook: ../../shared_rules/web_builders.yml

- hosts: www.osci.io
  tasks:
    - name: create web builder's user
      user:
        name: "{{ websync_user }}"
        comment: "Web Builder User"
    - name: "Create vhost for {{ inventory_hostname }}"
      include_role:
        name: httpd
        tasks_from: vhost
      vars:
        server_aliases:
          - osci.io
        document_root: "{{ websync_path }}"
        document_root_group: "{{ websync_user }}"
        use_tls: True
        use_letsencrypt: True
        force_tls: True
        hsts_include_subdomains: True
        hsts_preload: True
        content_security_policy: "default-src 'none'; font-src https://fonts.gstatic.com; img-src 'self' https://i.creativecommons.org https://licensebuttons.net; script-src 'self'; style-src 'self' 'unsafe-inline' https://fonts.googleapis.com/; frame-ancestors 'none'; base-uri 'self'; form-action 'self'"
  tags: main_website

- hosts: www.osci.io
  vars:
    data_dir: "{{ inventory_dir }}/data/tenants/osci/web"
    website_domain: himitsu.osci.io
    document_root: "/var/www/{{ website_domain }}"
    tenants_groups: "{{ groups | select('search', 'tenant_') | map('extract', groups) | list }}"
    tenants_info: "{{ (tenants_groups | map('first') | map('extract', hostvars) | selectattr('tenant', 'defined') | map(attribute='tenant') | selectattr('name', 'defined') | list + hostless_tenants.values() | list) | sort(attribute='name') }}"
  tasks:
    - name: "Fail if tenant group is empty"
      fail:
        msg: "Group for tenant '{{ item }}' is empty, please move it to `hostless_tenants`"
      when: groups[item]|length == 0
      loop: "{{ groups | select('search', 'tenant_') | list }}"
    - name: "Count hosts for tenants"
      set_fact:
        tenants_hosts: "{{ tenants_hosts|default({}) | combine({hostvars[item[0]].tenant.name|default(''): item|count}) }}"
      loop: "{{ tenants_groups }}"
    - name: "Create vhost for internal info"
      include_role:
        name: httpd
        tasks_from: vhost
      vars:
        use_tls: True
        use_letsencrypt: True
        force_tls: True
        content_security_policy: "default-src 'none'; font-src https://fonts.gstatic.com; img-src 'self' https://i.creativecommons.org https://licensebuttons.net; script-src 'self'; style-src 'self' 'unsafe-inline' https://fonts.googleapis.com/; frame-ancestors 'none'; base-uri 'self'; form-action 'self'"
        website_user: "{{ tenants_info_website.user }}"
        website_password: "{{ tenants_info_website.pwd }}"
    - name: "Install vhost specific configuration"
      template:
        src: "{{ data_dir }}/custom.conf"
        dest: "{{ _vhost_confdir }}/"
        owner: root
        group: "{{ httpd_usergroup }}"
        mode: 0644
      notify: reload httpd
    - name: "Create page for tenants info"
      template:
        src: "{{ data_dir }}/tenants_info.html"
        dest: "{{ document_root }}/"
        owner: root
        group: "{{ httpd_usergroup }}"
        mode: 0640
    - name: "Install assets for tenants info"
      copy:
        src: "{{ data_dir }}/{{ item }}"
        dest: "{{ document_root }}/"
        owner: root
        group: "{{ httpd_usergroup }}"
        mode: 0640
      loop:
        - tenants_info.css
        - headings_anchors.css
        - headings_anchors.js
  tags: tenants_info_website

- hosts: www.osci.io
  tasks:
    - name: "Create vhost for openshift-console.osci.io"
      include_role:
        name: httpd
        tasks_from: vhost
      vars:
        website_domain: openshift-console.osci.io
        redirect: https://console-openshift-console.apps.ospo-osci.z3b1.p1.openshiftapps.com/
        use_letsencrypt: True
        force_tls: True
  tags: openshift_console

# TLS for now, no DNS setup
- hosts: www.osci.io
  tasks:
    - name: "Create vhost for www.apacheweek.com"
      include_role:
        name: httpd
        tasks_from: vhost
      vars:
        website_domain: www.apacheweek.com
        document_root: /var/www/www.apacheweek.com
  tags: apacheweek

- hosts: catton.osci.io
  tasks:
    - name: Monitoring UI
      import_role:
        name: zabbix
        tasks_from: webui
      vars:
        title: "OSCI Supervision"
        website_domain: sup.osci.io
        use_letsencrypt: True
        tls: '{{ monitoring.tls }}'
        poller: '{{ monitoring.poller }}'
  tags: monitoring

